# Requirements

* [Angular CLI](https://cli.angular.io);
* [ng2-redux](github.com/angular-redux/ng2-redudx).

---

## How to use

Select the project you want to test.

```bash
# only one component. The simplest.
$ cd redux-demo/
$ npm install

$ ng serve
```

or

```bash
# more complex and real.
$ cd redux-todo-list-exercise/
$ npm install

$ ng serve
```

---

# Redux

* A library that helps you manage the state of your application;
* Is a simplified and lightweight implementation of **Flux** architecture that provides a clean an elegant solution to this problem: mantaining the application state in a predictable way;
* It is based in functional programming;
* Should be used only with large SPA.

## Benefits

* Predictable application state;
* Decoupled architecture - it allows you to postpone decisions about external libraries and frameworks which is one of the attributes of clean architecture;
* Testability - without Mocks, spies and any other tricks that can make testing both complex and error prone.

## Cost

* More code;
* More moving parts in your application;

Use redux only if you're building a medium to large simple page application (SPA) with complex views and data flows.

## Scenarios to use Redux

* Independent copies of the same data in multiple places;
* Multiple views that need to work with the same data and be in sync;
* Data can be updated by multiple users;
* Data can be updated by multiple actors.

---

In redux we have 3 pieces, 3 building blocks: **store, **actions** and **reducers**.

## Store

A single JS object that contains the state of the application.

```javascript
{
    nmessages: [...],
    newMessages: 5,
    chatSoundsEnabled: true
}
```

If different components need to work with the same slice, there is only one copy of that slice throughout the application. So, once a component modifies that slice, the changes are immediately visible to other components.

## Actions

Plain JS objects that represent something that has happened.

```javascript
{
    type: 'MARK_AS_READ'
}

{
    type: 'POST_MESSAGE',
    body: '...'
}
```

So these are simple data structures and don't have any logic. By **convention** use a type property to specify the **type** of each action. An action can have additionl **properties** related.

Every time we define a new action, we need to go to our reducer and determine how the state will change in response to that action.

## Reducer

It is basically a function that specifies how the state changes in response to an action. Think of it as an action handler or a event handler that determines how the state is changed.

A reducer does **not** modify the state. It returns the new state.

```javascript
// not allowed!
state.messages.push(...);
```

In redux, functions always take two arguments:

* **state**
* **action**

Based on **action type**, the return a new state, so tipically use a switch statement on the type property of the action and based on the value of **type property**, we return a **new state**.

```javascript
function reducer(state, action) {
    switch(action.type) {
        case 'INCREMENT':
            return { count: state.count +1 };
    }
}
```

Reducers should be **pure functions** that are:

* Same input -> Same output;
* No side effects;

We should not mutate or modify an of the arguments.

```javascript
// impure
function increment(input) {
    input.count ++; // mutating arguments
}

function increment(input) {
    service.addMessage(...);    // making backend calls
}

function increment(input) {
    input.count += Math.random();   // using an inpure function (that returns a different falue every time we call her)
}
```

```javascript
// pure
function increment(input) {
    return { count: input.count + 1};
}
```

### Benefits of not be allowed to mutate or modify state

* Easy testability;
* Ease undo/redo features beacuse always keep the previous state.

---

## Installing Redux

### Most popular Redux implementations for Angular

* [**ngrx/store**](github.com/ngx/store) has gone the route of re-implementing the Redux pattern in an Angular 2. It is not compatible with other libraries built for Redux.
* [**ng2-redux**](github.com/angular-redux/ng2-redudx) is built on top of the real redux library and it's compatible with much of the Redux ecosystem.


This project uses **ng2-redux**.


---

## Workflow with actions to update the state

### In the **app.component.ts**:

**1.** We injected *NgRedux* in the constructor;

**2.** In the **increment** method, instead of modifying the state directly, we dispatched an action;

### In the **store.ts**:

**3.** We modified the rootReducer and added support for the *increment* action as 

**4.** As part of this, we also had to modify the app state.

### In the **app.module.ts**:

**5.** Modify the constructor to set the initial state of the **store** with the existent properties.

---

## Update the view with the store value

### One way is subscribe the ngRedux and getState(), then update the value of the component with the value from the store:

```javascript
constructor(private ngRedux: NgRedux<AppState>) {
    // we need to read the state
    const subscription = ngRedux.subscribe(() => {
      // return the current state of the store
      const store = ngRedux.getState();
      // update the counter
      this.counter = store.counter;

    });
}
```

However, this implementation has a problem. Since is a subscription, we should handle **on destroy** lifecycle of the component. So, whenever our component is destroyed, we should make sure to **unsubscraibe** from tthis subscription, otherwise we're going end up with **memory leaks**.

### With other way, **better**, we can select a slice of the store as an *observable* and then in the **template** we can use the *async* pipe to unwrap it. With this, we don't need to explicity unsubscribe from that subscription.


```javascript
// app.component.ts

// with select, we can select a slice of the store as an observable
import { NgRedux, select } from 'ng2-redux';

export class AppComponent {
    
    // include @select decorator to return an observable, not a number. It is a slice of the store
    // by convention the name of this field looks up for a field with the exact same name in the store
    // if you want a different name, specify the name @ store inside @select('here')
    @select() counter;

    constructor(private ngRedux: NgRedux<AppState>) {

    }
}
```

```html
<!-- app.component.html -->
<p>Counter: {{ counter | async }}</p>
```

---

## Avoid Object Mutations

In the store, inside the **rootReducer** method, there are 3 ways to update the properties:

```javascript
/**
 * @description reducer function.
 *  We can break into smaller more maintainable functions
 *  each focusing on one domain in the application.
 *
 *  Every time we define a new action, we need to go to our reducer
 *      and determine how the state will change in response to that action.
 *
 * @param state - the current state
 * @param action
 *
 * @returns new state
 */
export function rootReducer(state: AppState, action ): AppState {
    switch (action.type) {
        case INCREMENT:
            /*
             * with this method, you have to assign every single property
             * */
            // return { counter: state.counter + 1 };

            /*
             * we can use Object.sign() to take a copy of an object
             * we can combine multiple objects in one object
             * Object.sign(targetObject, sourceObject, anotherObject)
             * we apply the mutation on the 3rd argument
             * along with this 'counter' property, will be combined and placed into the 1st object
             * */
            // return Object.assign({}, state, {counter: state.counter + 1} );
            /*
             * disadvantage: in the 3rd argument, I can add a property that does not exist in the state
             * */

            /**
             * solution: use tassign, a typesafe version of Object.assign()
             */
            return tassign(state, { counter: state.counter + 1});
    }
    return state;
}

```

You need to install tassign (not in this project anymore, is already here):

```bash
$ npm install tassign --save
```

---

## Using Immutable Objects - not necessarily the best option...

### Advantage

This allows to prevent accidental mutations.

### Disadvantages

* the code is **more verbose**;
* **can't access** properties using dot notation,
* **have to** work with *get()*, *set()* methods and some *magic strings* inside;
* the *interface* we made becames **useless**.

```bash
$ npm install immutable --save
```

### In **app.module.ts**

Import the needful from `immutable`.

```javascript
import { fromJS, Map } from 'immutable';
```

Convert the plain JavaScript object (INITIAL_STATE) to a Immutable Object inside the AppModule constructor, using **fromJS**. This returns a map type, so the constructor must return something like **Map** where the **key** is a *string*, and the **value** can be *anything*.

```javascript
constructor(ngRedux: NgRedux<Map<string, any>>) {

    ngRedux.configureStore(rootReducer, fromJS(INITIAL_STATE));
  }
```

### In **store.ts**
You need to choose between using **tassign** approach or **immutable objects**. They both have srengths and weaknesses.

The **rootReducer** method must return a map.

The method always returns a new object. It keeps the original state unmodified.

```javascript
import { Map } from 'immutable';

export function rootReducer(state: Map<string, any>, action ): Map<string, any> {
    switch (action.type) {
        case INCREMENT:
            return state.set('counter', state.get('counter') +1);
    }
    return state;
}

```

In **app.component.ts**

Update the @select() count.

```javascript
@select( s => s.get('counter')) counter;  // to work with immutable objects
```

---

# Redux DevTools

Is a [Chorme extension](https://chrome.google.com/webstore/search/redux%20devtools?hl=en). Also available for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/remotedev/).

In your **app.module.ts** you should import the extension and inject in the constructor:

```javascript
// app.module.ts

import { DevToolsExtension } from 'ng2-redux';

// ...

export class AppModule {
  constructor(
      devTools: DevToolsExtension, 
      ngRedux: NgRedux<IAppState>){
    // ngRedux.configureStore(rootReducer method, initial state, middleware**, enhancements***)
    ngRedux.configureStore(rootReducer, INITIAL_STATE, [], [devTools.enhancer()]);
  }
 }

 /*
 ** is an extension point, so we can execute some code from the moment an action is
 dispatched to the moment it reaches a reducer.
 Example: logging, where we can log every action and do something about it

 *** that's where we're going to use the DevTools extension
 */
```

This has some cost. We're going to run this only when the application is in **development mode**, so:

```javascript
// app.module.ts

// do this
import { isDevMode } from '@angular/core';

// ...

export class AppModule {
  constructor(
      devTools: DevToolsExtension, 
      ngRedux: NgRedux<IAppState>){
    
    // dos this
    const enhancers = isDevMode() ? [devTools.enhancer()] : []

    ngRedux.configureStore(rootReducer, INITIAL_STATE, [], [enhancers]);
  }
 }

```

# Improvements

* Calling APIs

* Dealing with Complex Domain

* Refactoring Fat Case Statements

For this points, take a look to this [videos](https://bitbucket.org/marcocsp/redux-in-angular/src/e469ed295b7e599005bd9d53ce1b8c6ced848a0b/redux-todo-list-exercise/video/?at=master).
