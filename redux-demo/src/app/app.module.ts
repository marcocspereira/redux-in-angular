import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// 1st step import types from ng2-redux
import { NgRedux, NgReduxModule } from 'ng2-redux';


import { AppState, rootReducer, INITIAL_STATE } from './store';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgReduxModule // 2nd step - import to use for dependency injection
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

  // 3rd step
  constructor(ngRedux: NgRedux<AppState>) {
    // 4th step - initialize the store(<reducer function>, <initial store>)
    ngRedux.configureStore(rootReducer, INITIAL_STATE);

  }
}
