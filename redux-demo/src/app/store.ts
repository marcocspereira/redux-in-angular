import { INCREMENT } from './actions';
import { tassign } from 'tassign';

/**
 * @description shape of our STORE.
 */

export interface AppState {
    counter: number;

    messaging?: {
        newMessages: number
    };
}

export const INITIAL_STATE: AppState = {
    counter: 0,
    messaging: {
        newMessages: 5
    }
};


/**
 * @description reducer function.
 *  We can break into smaller more maintainable functions
 *  each focusing on one domain in the application.
 *
 *  Every time we define a new action, we need to go to our reducer
 *      and determine how the state will change in response to that action.
 *
 * @param state - the current state
 * @param action
 *
 * @returns new state
 */
export function rootReducer(state: AppState, action ): AppState {
    switch (action.type) {
        case INCREMENT:
            // return { counter: state.counter + 1 };

            // return Object.assign({}, state, {counter: state.counter + 1} );

            return tassign(state, { counter: state.counter + 1});
    }
    return state;
}
