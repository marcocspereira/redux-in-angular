import { Component } from '@angular/core';

// 1) import NgRedux
// with select, we can select a slice of the store as an observable
import { NgRedux, select } from 'ng2-redux';
import { AppState } from './store';

import { INCREMENT } from './actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  // include @select decorator to return an observable, not a number. It is a slice of the store
  // by convention the name of this field looks up for a field with the exact same name in the store
  // if you want a different name, specify the name @ store inside @select('here')
  @select() counter;

  @select(['messaging', 'newMessages']) newMessages;
  // ^ we can access messagin.newMessages in our store
  @select( (s: AppState) => s.messaging.newMessages) newMessagesCount;

  // 2) inject NgRedux with AppState interface, the shape of our store.
  constructor(private ngRedux: NgRedux<AppState>) {
    /* one way to update the template with the value from the store

    // we need to read the state
    const subscription = ngRedux.subscribe(() => {
      // return the current state of the store
      const store = ngRedux.getState();
      console.log(store);

      this.counter = store.counter;

    }); */


  }

  increment() {
    // typical angular app modifies the state directly here
    // this.counter++;

    // ------------------ Redux way ------------------

    // with Redux architecture, we didn't modify the state here.

    // Instead we dispatch an action that goes in the store.

    // The store knows our rootReducer so it passes the action to the
    //    rootReducer and then the reducer looks at the action

    // Based on the type of the action, it will return a new state
    //    and then the store will update its state iternally

    this.ngRedux.dispatch(
      {
        type: INCREMENT/* ,
        body: '',
        subject: '' */
      }
    ); // pass an action object
  }
}
