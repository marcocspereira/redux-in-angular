import { BrowserModule } from '@angular/platform-browser';
import { NgModule, isDevMode } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgRedux, NgReduxModule, DevToolsExtension } from 'ng2-redux';

import { AppComponent } from './app.component';
import { IAppState, rootReducer, INITIAL_STATE } from './store';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoDashboardComponent } from './todo-dashboard/todo-dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    TodoDashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgReduxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(
      devTools: DevToolsExtension,
      ngRedux: NgRedux<IAppState>){

    const enhancers = isDevMode() ? [devTools.enhancer()] : [];
    // ngRedux.configureStore(rootReducer method, initial state, middleware**, enhancements***)
    ngRedux.configureStore(rootReducer, INITIAL_STATE, [], enhancers);
  }
 }


/*
 ** is an extension point, so we can execute some code from the moment an action is
 dispatched to the moment it reaches a reducer.
 Example: logging, where we can log every action and do something about it

 *** that's where we're going to use the DevTools extension
 */